#!/usr/bin/env python
# coding: utf-8

# In[1]:


import numpy as np
from scipy.integrate import odeint


# Имеем систему
# 
# 
# $
# \begin{cases}
# \dot{m} = r m(t) - \theta \widetilde{m}(t) ln m(t) - \gamma m(t) f(h(t))
# \\ \dot{h} = -\alpha h(t) + u(t)
# \end{cases}
# $

# In[2]:


r = 0.18
T = 30
m_0 = 5 * 10**7
h_0 = 0
theta = 0.01
gamma = 1
alpha = 0.03
Q_0 = 0.8


# In[3]:


def f(h):
    return h * np.exp(-h / 2)

def h_tilda_fun(t, u):
    return u / alpha + (h_tilda_0 - u / alpha) * np.exp(-alpha * t)


# Не будем проводить зашумление данных.

# In[4]:


def get_truncated_normal(mu, sigma):
    return list(filter(lambda x: 0 < x < 2, np.random.normal(mu, sigma, 100)))[0]

normal_noise_1 = get_truncated_normal(0, 1)
h_tilda_0 = 0 # + get_truncated_normal(0, 1)
print("Noise for h:", normal_noise_1)

normal_noise_2 = get_truncated_normal(0, 1)
m_tilda_0 = m_0 # + normal_noise_2
print("Noise for m:", normal_noise_2)


# Решим ОДУ $\dot{h} = -\alpha h(t) + Q_0$ численно

# In[5]:



def model_h_tilda(h, t):
    dhdt = -alpha * h + Q_0
    return dhdt

t = np.linspace(0, 3, num=2000)
h_tilda = odeint(model_h_tilda, h_tilda_0, t)


# In[6]:


import matplotlib.pyplot as plt
get_ipython().magic(u'matplotlib inline')

plt.rcParams.update({'font.size': 22})


# Сравним решение $h(t)$ с аналитическим решением

# In[7]:


plt.figure(figsize=(10, 10))
plt.plot(t, h_tilda, label='h_tilda_odeint(t)')
plt.plot(t, list(map(lambda t: h_tilda_fun(t, Q_0), t)), label='h_tilda_handint(t)')
plt.hlines(0, 0, 3)
plt.vlines(0, 0, 3)
plt.hlines(2, -1, 3, colors='r', label='h = 2')
plt.xlabel('t')
plt.ylabel('h(t)')
plt.axis('equal')
plt.legend()


# 
# Как мы видим, решения совпали.

# 
# 
# Покажем поведение $h(t)$ на длительном промежутке времени.

# Найдём момент переключения $\widetilde{\tau}$

# In[8]:


from scipy.optimize import root_scalar

tau_tilda = root_scalar(lambda t: h_tilda_fun(t, Q_0) - 2, bracket=[0, 3]).root
print("tau_tilda: ", tau_tilda)


# In[9]:


t_long = np.linspace(0, 30)

plt.figure(figsize=(12, 12))
plt.xlabel('t')
plt.ylabel('h(t)')
plt.axis('equal')
plt.title("Behavior of h in the long run.")
plt.legend()
plt.plot(t_long, list(map(lambda t: 2 if t > tau_tilda else h_tilda_fun(t, Q_0), t_long)), label='h_tilda_fixed(t)')


#     
# Исследуем $\widetilde{m}(t)$ на промежутке $(0, \widetilde{\tau})$ 
#     

# In[10]:


def model_m_left(m, t):
    dmdt = r * m - theta * m * np.log(m) - gamma * m * f(h_tilda_fun(t, Q_0))
    return dmdt

t_left = np.linspace(0, tau_tilda)
m_tilda_left = odeint(model_m_left, m_tilda_0, t_left)


# In[11]:


plt.figure(figsize=(12, 12))
plt.plot(t_left, m_tilda_left, label='m(t)')
plt.xlabel('t')
plt.ylabel('m(t)')
plt.title("m_tilda at (0, tau_tilda)")
plt.legend()


#     
# Исследуем $\widetilde{m}(t)$ на промежутке $(\widetilde{\tau}, T)$ 

# In[12]:


def model_m_right(m, t):
    dmdt = r * m - theta * m * np.log(m) - gamma * m * f(2)
    return dmdt

t_right = np.linspace(tau_tilda, T)
m_tilda_right = odeint(model_m_right, m_tilda_left[-1], t_right)


# In[13]:


plt.figure(figsize=(12, 12))
plt.plot(t_right, m_tilda_right, label='m(t)')

plt.xlabel('t')
plt.ylabel('m(t)')
plt.title("m_tilda at (tau_tilda, T)")
plt.legend()


# In[14]:


t_whole = np.concatenate((t_left, t_right), axis=0)
m_tilda_whole = np.concatenate((m_tilda_left, m_tilda_right), axis=0)


# In[15]:


plt.rcParams.update({'font.size': 22})
plt.figure(figsize=(14, 5))
plt.plot(t_whole, m_tilda_whole, label='m(t)')
plt.vlines(tau_tilda, 0, 5e7, colors='r')
plt.xlabel('t')
plt.ylabel('m(t)')
plt.legend()


# In[16]:


print("Answer with fixed tau_tilda = 2", m_tilda_whole[-1])


# In[ ]:





# In[ ]:





# In[ ]:




