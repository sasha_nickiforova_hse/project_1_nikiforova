#!/usr/bin/env python
# coding: utf-8

# In[3]:


import numpy as np
from scipy.integrate import odeint


# Имеем систему
# 
# 
# $
# \begin{cases}
# \dot{m} = r m(t) - \theta \widetilde{m}(t) ln m(t) - \gamma m(t) f(h(t))
# \\ \dot{h} = -\alpha h(t) + u(t)
# \end{cases}
# $

# In[4]:



r = 0.18
T = 30
m_0 = 5 * 10**7
h_0 = 0
theta = 0.01
gamma = 1
alpha = 0.03
Q_0 = 0.8


# In[5]:


def f(h):
    return h * np.exp(-h / 2)


def h_tilda_fun(t, u, h_tilda_0):
    return u / alpha + (h_tilda_0 - u / alpha) * np.exp(-alpha * t)


# Не будем проводить зашумление данных.

# In[6]:


def get_truncated_normal(mu, sigma):
    return list(filter(lambda x: 0 < x < 2, np.random.normal(mu, sigma, 100)))[0]

num_samples = 30
normal_noise_1 = get_truncated_normal(0, 1)
h_tilda_0_list = list(map(lambda _: 0 + get_truncated_normal(0, 1), range(num_samples)))

normal_noise_2 = get_truncated_normal(0, 1)
m_tilda_0_list = list(map(lambda _: m_0 + get_truncated_normal(0, 1), range(num_samples)))


# Решим ОДУ $\dot{h} = -\alpha h(t) + Q_0$ численно

# In[7]:


def model_h_tilda(h, t):
    dhdt = -alpha * h + Q_0
    return dhdt

t = np.linspace(0, 3, num=2000)

h_tilda_list = list()
for h_tilda_0 in h_tilda_0_list:
    h_tilda = odeint(model_h_tilda, h_tilda_0, t)
    h_tilda_list.append(h_tilda)


# In[8]:


import matplotlib.pyplot as plt
get_ipython().magic(u'matplotlib inline')

plt.rcParams.update({'font.size': 22})


# Сравним решение $h(t)$ с аналитическим решением

# In[9]:


plt.figure(figsize=(10, 10))
for h_tilda in h_tilda_list:
    plt.plot(t, h_tilda, label='')
plt.hlines(0, 0, 3)
plt.vlines(0, 0, 3)
plt.hlines(2, -1, 3, colors='r', label='h = 2')
plt.xlabel('t')
plt.ylabel('h(t)')
plt.axis('equal')
plt.legend()


# 
# Как мы видим, решения совпали.
# 

# Найдём момент переключения $\widetilde{\tau}$

# In[10]:


from scipy.optimize import root_scalar
import pandas as pd


tau_tilda_list = list()
for h_tilda_0 in h_tilda_0_list:
    tau_tilda = root_scalar(lambda t: h_tilda_fun(t, Q_0, h_tilda_0) - 2, bracket=[0, 3]).root
    tau_tilda_list.append(tau_tilda)
    
tau_tilda_series = pd.Series(tau_tilda_list, name='tau_tilda')
tau_tilda_series.to_csv("tau_tilda.csv", index=False, header=False)

print("Tau_tilda mean:", np.mean(tau_tilda_list))


# Покажем поведение $h(t)$ на длительном промежутке времени.

# In[11]:


t_long = np.linspace(0, 5, num=200)

plt.figure(figsize=(12, 12))
for h_tilda_0, tau_tilda in zip(h_tilda_0_list, tau_tilda_list):
    plt.plot(t_long, list(map(lambda t: 2 if t > tau_tilda else h_tilda_fun(t, Q_0, h_tilda_0), t_long)))
plt.xlabel('t')
plt.ylabel('h(t)')
plt.axis('equal')
plt.title("Behavior of h in the long run.")
plt.legend()


# In[ ]:





#     
# Исследуем $\widetilde{m}(t)$ на промежутке $(0, \widetilde{\tau})$ 
#     

# In[12]:


t_left_list = list()
m_tilda_left_list = list()
for tau_tilda, m_tilda_0, h_tilda_0 in zip(tau_tilda_list, m_tilda_0_list, h_tilda_0_list):
    model_m_left = lambda m, t: r * m - theta * m * np.log(m) - gamma * m * f(h_tilda_fun(t, Q_0, h_tilda_0))
    t_left = np.linspace(0, tau_tilda)
    m_tilda_left = odeint(model_m_left, m_tilda_0, t_left)
    
    t_left_list.append(t_left)
    m_tilda_left_list.append(m_tilda_left)


# In[13]:


plt.figure(figsize=(12, 12))
for t_left, m_tilda_left in zip(t_left_list, m_tilda_left_list):
    plt.plot(t_left, m_tilda_left)
    
plt.xlabel('t')
plt.ylabel('m(t)')
plt.title("m_tilda at (0, tau_tilda)")
plt.legend()


#     
# Исследуем $\widetilde{m}(t)$ на промежутке $(\widetilde{\tau}, T)$ 

# In[14]:


def model_m_right(m, t):
    dmdt = r * m - theta * m * np.log(m) - gamma * m * f(2)
    return dmdt

t_right_list = list()
m_tilda_right_list = list()
for tau_tilda, m_tilda_left in zip(tau_tilda_list, m_tilda_left_list):
    t_right = np.linspace(tau_tilda, T)
    m_tilda_right = odeint(model_m_right, m_tilda_left[-1], t_right)
    
    t_right_list.append(t_right)
    m_tilda_right_list.append(m_tilda_right)


# In[15]:


plt.figure(figsize=(12, 12))
for t_right, m_tilda_right in zip(t_right_list, m_tilda_right_list):
    plt.plot(t_right, m_tilda_right)

plt.xlabel('t')
plt.ylabel('m(t)')
plt.title("m_tilda at (tau_tilda, T)")
plt.legend()


# In[16]:


t_whole_list = list()
for t_left, t_right in zip(t_left_list, t_right_list):
    t_whole = np.concatenate((t_left, t_right), axis=0)
    t_whole_list.append(t_whole)
    
m_tilda_whole_list = list()
for m_tilda_left, m_tilda_right in zip(m_tilda_left_list, m_tilda_right_list):
    m_tilda_whole = np.concatenate((m_tilda_left, m_tilda_right), axis=0)
    m_tilda_whole_list.append(m_tilda_whole)


# In[17]:


plt.figure(figsize=(14, 5))
for t_whole, m_tilda_whole in zip(t_whole_list, m_tilda_whole_list):
    plt.plot(t_whole, m_tilda_whole)
plt.xlabel('t')
plt.ylabel('m(t)')
plt.legend()


# In[16]:


import seaborn as sns

m_tilda_endpoints = list(map(lambda m_tilda: m_tilda[-1], m_tilda_whole_list))
sns.distplot(m_tilda_endpoints)


# In[17]:


import os
os.path.abspath(os.getcwd())


# In[ ]:





# In[ ]:




